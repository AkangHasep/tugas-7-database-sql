
Soal 1 Membuat Database
 
buat database myshop:
CREATE DATABASE myshop;

 

Soal 2 Membuat Table di Dalam Database

buat tabel users:

CREATE TABLE users ( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255), email varchar(255), password varchar(255) );


buat tabel categories:

CREATE TABLE categories( id int PRIMARY key AUTO_INCREMENT, name varchar(255) );


buat tabel items:

CREATE TABLE items( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255), description varchar(255), price int, stock int, category_id int, FOREIGN KEY (category_id) REFERENCES categories(id) );



Soal 3 Memasukkan Data pada Table

memasukkan data ke tabel users:
INSERT INTO users (name, email, password) VALUES ('John Doe', 'john@doe.com', 'john123'), ('Jane Doe', 'jane@doe.com', 'jenita123');


memasukkan data ke tabel categories:
INSERT INTO categories (name) VALUES ('gadget'), ('cloth'), ('men'), ('women'), ('branded');


memasukkan data ke tabel items:
INSERT INTO items (name, description, price, stock, category_id) VALUES ('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1), ('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2), ('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);




Soal 4 Mengambil Data dari Database
 
a. Mengambil data users, kecuali data password:
	SELECT id, name, email FROM users;

b. Mengambil data items
   - harga di atas 1000000 (satu juta):
	SELECT * FROM items WHERE price >1000000;

   - data yang memiliki name serupa atau mirip (like) dengan kata kunci �uniklo�, �watch�, atau �sang� (pilih salah satu saja).
	SELECT * FROM items WHERE name LIKE "%watch";
 

c. Menampilkan data items join dengan kategori
	SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name AS kategori FROM items INNER JOIN categories ON items.category_id = categories.id;



Soal 5 Mengubah Data dari Database

ubah harga sumsang b50 di tabel items:
UPDATE items set price=2500000 where id=1;
